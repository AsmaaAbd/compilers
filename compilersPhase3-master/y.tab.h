/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 64 "syntax.y" /* yacc.c:1909  */

	#include <vector>
	using namespace std;

#line 49 "y.tab.h" /* yacc.c:1909  */

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    ARITH_OP = 258,
    REL_OP = 259,
    BOOL_OP = 260,
    IDENTIFIER = 261,
    INT = 262,
    FLOAT = 263,
    BOOL = 264,
    IF_WORD = 265,
    ELSE_WORD = 266,
    WHILE_WORD = 267,
    FOR_WORD = 268,
    INT_WORD = 269,
    FLOAT_WORD = 270,
    BOOLEAN_WORD = 271,
    SEMI_COLON = 272,
    EQUALS = 273,
    LEFT_BRACKET = 274,
    RIGHT_BRACKET = 275,
    LEFT_BRACKET_CURLY = 276,
    RIGHT_BRACKET_CURLY = 277,
    COMMA = 278,
    SYSTEM_OUT = 279
  };
#endif
/* Tokens.  */
#define ARITH_OP 258
#define REL_OP 259
#define BOOL_OP 260
#define IDENTIFIER 261
#define INT 262
#define FLOAT 263
#define BOOL 264
#define IF_WORD 265
#define ELSE_WORD 266
#define WHILE_WORD 267
#define FOR_WORD 268
#define INT_WORD 269
#define FLOAT_WORD 270
#define BOOLEAN_WORD 271
#define SEMI_COLON 272
#define EQUALS 273
#define LEFT_BRACKET 274
#define RIGHT_BRACKET 275
#define LEFT_BRACKET_CURLY 276
#define RIGHT_BRACKET_CURLY 277
#define COMMA 278
#define SYSTEM_OUT 279

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 71 "syntax.y" /* yacc.c:1909  */

	char* aopval;		// string represents the type of the operation (e.g "+")
	int intval;
	float floatval;
	bool boolval;	
	char * idval;
	struct {
		int sType;
	} expr_type;
	struct {
		int false_;
		int true_;
	} bexpr_type;
	struct {
		int end;
	} stmt_type;
	struct {
		int end; 
		int else_;
	} if_type;
	struct {
		int exp_label; 
		int end;
	} while_type;
	int sType;

#line 136 "y.tab.h" /* yacc.c:1909  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
