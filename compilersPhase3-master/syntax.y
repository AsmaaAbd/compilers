%{
#include <fstream>
#include <iostream>
#include <map>
#include <cstring>
#include <vector>
#include <stdio.h>
#include <unistd.h>
#include "bytecode_inst.h"
#define GetCurrentDir getcwd

using namespace std;
extern int yylex();
extern FILE *yyin;
void yyerror(const char * s);
int lineCounter = 0;

#define TRUE 1
#define FALSE 0
string outfileName ;

int varaiblesNum = 1;
int labelsCount = 1;
int temp_if_label = 0;
int temp_while_label = 0;	
string inFile ;
ofstream fout("output.j");



typedef enum {INT_T, FLOAT_T, BOOL_T, VOID_T, ERROR_T} type_enum;

map<string, pair<int,type_enum> > symTab;




//extern  int yyparse();


//void cast (string x, int type_t1);///
//string genLabel(); ///
//void backpatch(vector<int> *list, int num);///
//string getLabel(int n);///
//vector<int> * merge (vector<int> *list1, vector<int>* list2);///

string getOp(string op);
void header(void);	
void footer(void);	
void arithCast(int from , int to, string op);
bool checkId(string id);
void defineVar(string name, int type);
vector<string> codeList;
void writeCode(string x);
void printCode(void);

%}






%code requires {
	#include <vector>
	using namespace std;
}

%start method_body

%union{
	char* aopval;		// string represents the type of the operation (e.g "+")
	int intval;
	float floatval;
	bool boolval;	
	char * idval;
	struct {
		int sType;
	} expr_type;
	struct {
		int false_;
		int true_;
	} bexpr_type;
	struct {
		int end;
	} stmt_type;
	struct {
		int end; 
		int else_;
	} if_type;
	struct {
		int exp_label; 
		int end;
	} while_type;
	int sType;
}

// Tokens defined in lexical


%token <aopval> ARITH_OP
%token <aopval> REL_OP
%token <aopval> BOOL_OP
%token <idval> IDENTIFIER
%token <intval> INT
%token <floatval> FLOAT
%token <boolval> BOOL
%token IF_WORD
%token ELSE_WORD
%token WHILE_WORD
%token FOR_WORD
%token INT_WORD
%token FLOAT_WORD
%token BOOLEAN_WORD
%token SEMI_COLON
%token EQUALS
%token LEFT_BRACKET
%token RIGHT_BRACKET
%token LEFT_BRACKET_CURLY
%token RIGHT_BRACKET_CURLY
%token COMMA
//%token NEW_LINE

%token SYSTEM_OUT

%type <sType> primitive_type
%type <expr_type> expression
%type <bexpr_type> b_expression
%type <stmt_type> statement
%type <stmt_type> statement_list
%type <if_type> if
%type <while_type> while



%% 
method_body
	: 
	{header();}
	statement_list
	{footer();}
	;

statement_list
	: 
	statement{if($1.end != 0 ){writeCode("L" + to_string($1.end) + ":");}$$.end = 0;} 
	| 
	statement_list statement{if($2.end != 0 ){writeCode("L" + to_string($2.end)+":");}}
	;

statement: 
	declaration {$$.end = 0;}
	|if {$$.end = $1.end;}
	|while {$$.end = $1.end;}
	| assignment {$$.end = 0;}
	| system_print {$$.end = 0;}
	;
	
declaration: 
	primitive_type IDENTIFIER SEMI_COLON /* implement multi-variable declaration */
	{
		string str($2);
		if($1 == INT_T)
		{
			defineVar(str,INT_T);
		}else if ($1 == FLOAT_T)
		{
			defineVar(str,FLOAT_T);
		}
	}
	;
	
primitive_type: 
	INT_WORD {$$ = INT_T;}
	| FLOAT_WORD {$$ = FLOAT_T;}
	|BOOLEAN_WORD {$$ = BOOL_T;}
	;

if: 
	IF_WORD LEFT_BRACKET 
	b_expression
	RIGHT_BRACKET LEFT_BRACKET_CURLY
	statement_list
	{
	$<stmt_type>$.end = labelsCount;
	writeCode("goto L" + to_string(labelsCount++));
	}
	RIGHT_BRACKET_CURLY 
	ELSE_WORD LEFT_BRACKET_CURLY
	{
	  writeCode("L" + to_string($3.false_) + ":");
	}
	statement_list
	RIGHT_BRACKET_CURLY
	{$$.end = $<stmt_type>7.end;}
	;

while:
	WHILE_WORD LEFT_BRACKET
	{
	  $<stmt_type>$.end = labelsCount;
	  writeCode("L" + to_string(labelsCount++) + ":");
	  
	 // temp_while_label = labelsCount++;
	}
	b_expression
	RIGHT_BRACKET LEFT_BRACKET_CURLY
	statement_list
	{
	  writeCode("goto L" + to_string($<stmt_type>3.end) );
	}
	RIGHT_BRACKET_CURLY
	{
	  $$.exp_label = $<stmt_type>3.end;
	  $$.end = $4.false_;
	}
	;

assignment: 
	IDENTIFIER EQUALS expression SEMI_COLON
	{
		string str($1);
		/* after expression finishes, its result should be on top of stack. 
		we just store the top of stack to the identifier*/
		if(checkId(str))
		{
				if($3.sType == INT_T)
				{
					writeCode("istore " + to_string(symTab[str].first));
				}else if ($3.sType == FLOAT_T)
				{
					writeCode("fstore " + to_string(symTab[str].first));
				}
		}else{
			string err = "identifier: "+str+" isn't declared in this scope";
			yyerror(err.c_str());
		}
	}
	;
expression: 
	FLOAT 	{$$.sType = FLOAT_T; writeCode("ldc "+to_string($1));}
	| INT 	{$$.sType = INT_T;  writeCode("ldc "+to_string($1));} 
	| expression ARITH_OP expression	{arithCast($1.sType, $3.sType, string($2));}
	| IDENTIFIER {
		string str($1);
		if(checkId(str))
		{
			$$.sType = symTab[str].second;
			if(symTab[str].second == INT_T)
			{
				writeCode("iload " + to_string(symTab[str].first));
			}else if (symTab[str].second == FLOAT_T)
			{
				writeCode("fload " + to_string(symTab[str].first));
			}
		}
		else
		{
			string err = "identifier: "+str+" isn't declared in this scope";
			yyerror(err.c_str());
			$$.sType = ERROR_T;
		}
	}
	| LEFT_BRACKET expression RIGHT_BRACKET {$$.sType = $2.sType;}
	;
system_print:
	SYSTEM_OUT LEFT_BRACKET expression RIGHT_BRACKET SEMI_COLON
	{
		if($3.sType == INT_T)
		{
			/* expression is at top of stack now */
			/* save it at the predefined temp syso var */			
			writeCode("istore " + to_string(symTab["1syso_int_var"].first));
			/* call syso */			
			writeCode("getstatic      java/lang/System/out Ljava/io/PrintStream;");
			/*insert param*/
			writeCode("iload " + to_string(symTab["1syso_int_var"].first ));
			/*invoke syso*/
			writeCode("invokevirtual java/io/PrintStream/println(I)V");

		}else if ($3.sType == FLOAT_T)
		{
			writeCode("fstore " + to_string(symTab["1syso_float_var"].first));
			/* call syso */			
			writeCode("getstatic      java/lang/System/out Ljava/io/PrintStream;");
			/*insert param*/
			writeCode("fload " + to_string(symTab["1syso_float_var"].first ));
			/*invoke syso*/
			writeCode("invokevirtual java/io/PrintStream/println(F)V");
		}
	}
	;
	
b_expression:
	BOOL
	{
		if(! $1){
			writeCode("goto " + to_string(labelsCount) );
			$$.false_ = labelsCount++ ; 
		}
	}	
	| expression REL_OP expression		
	{
		string op ($2);
		writeCode( getOp(op)+ " L" + to_string(labelsCount) );
		$$.false_ = labelsCount++ ; 
	}
	/*|expression REL_OP BOOL 	// to be considered */ 
	;
%%





/*------------------------------separator------------------------------------------------*/

main (int argv, char * argc[])
{
	FILE *myfile;
	if(argv == 1) 
	{
		myfile = fopen("input_code.txt", "r");
		inFile = "input_code.txt";
	}
	else 
	{
		myfile = fopen(argc[1], "r");
		inFile = string(argc[1]);
	}
	if (!myfile) {
		printf("I can't open input code file!\n");
		char cCurrentPath[200];
		 if (!GetCurrentDir(cCurrentPath, sizeof(cCurrentPath)))
		     {
		     return -1;
		     }
		printf("%s\n",cCurrentPath);  
				getchar();

		return -1;

	}
	yyin = myfile;
	yyparse();
	//getchar();
	printCode();
}





void cast (string str, int t1)
{
	yyerror("casting not implemented yet :)");
}

void arithCast(int from , int to, string op)
{
	if(from == to)
	{
		if(from == INT_T)
		{
			writeCode("i" + getOp(op));
		}else if (from == FLOAT_T)
		{
			writeCode("f" + getOp(op));
		}
	}
	else
	{
		yyerror("cast not implemented yet");
	}
}




bool checkId(string op)
{
	return (symTab.find(op) != symTab.end());
}

void defineVar(string name, int type)
{
	if(checkId(name))
	{
		string err = "variable: "+name+" declared before";
		yyerror(err.c_str());
	}else
	{
		if(type == INT_T)
		{
			writeCode("iconst_0\nistore " + to_string(varaiblesNum));
		}
		else if ( type == FLOAT_T)
		{
			writeCode("fconst_0\nfstore " + to_string(varaiblesNum));
		}
		symTab[name] = make_pair(varaiblesNum++,(type_enum)type);
	}
}

void writeCode(string x)
{
	codeList.push_back(x);
}

void printCode(void)
{
	for ( int i = 0 ; i < codeList.size() ; i++)
	{
		fout<<codeList[i]<<endl;
	}
}



// YY Methods
void yyerror(const char *s)
{
	printf("error@%d: %s\n",lineCounter, s);
}



// Helper Functions

string getOp(string op)
{
	if(inst_byte_code.find(op) != inst_byte_code.end())
	{
		return inst_byte_code[op];
	}
	return "";
}

void header()
{
	writeCode(".source " + inFile);
	writeCode(".class public test"); 
	writeCode(".super java/lang/Object");
	
	writeCode(".method public <init>()V");
	writeCode("aload_0");
	writeCode("invokenonvirtual java/lang/Object/<init>()V");
	writeCode("return");
	writeCode(".end method");

	writeCode(".method public static main([Ljava/lang/String;)V");
	writeCode(".limit locals 100");
	writeCode(".limit stack 100");

	/* generate temporal vars for syso*/
	defineVar("1syso_int_var",INT_T);
	defineVar("1syso_float_var",FLOAT_T);

	/*generate line*/
	//writeCode(".line 1");
}

void footer()
{
	writeCode("return");
	writeCode(".end method");
}
