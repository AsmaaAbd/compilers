#ifndef _____BYTE_CODE_INSTRUCTIONS
#define _____BYTE_CODE_INSTRUCTIONS

#include <map>
using namespace std;

/* list of mnemonics that corresponds to specific operators */
map<string,string> inst_byte_code = {
	/* Arithmetic Operations */
	{"+", "add"},
	{"-", "sub"},
	{"/", "div"},
	{"*", "mul"},

	/* Relational operations */
	{"==", "if_icmpne"},
	{"<=", "if_icmpgt"},
	{">=", "if_icmplt"},
	{"!=", "if_icmpeq"},
	{">",  "if_icmple"},
	{"<",  "if_icmpge"}
};

#endif
